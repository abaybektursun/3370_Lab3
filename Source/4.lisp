(defun my-recursive-remove(an-atom a-list)
	(let (new-list (list))
		
		(loop for an-item in a-list
			do(
				if (typep an-item 'list)
					(progn
						(push (my-recursive-remove an-atom an-item) new-list)
					)
					(
						if (equal an-atom an-item)
						(print "Found, skip")
						(push an-item new-list)
					)
			)
		)
		new-list
	)
)

(print (my-recursive-remove 'a '(a b c d)) )
(print (my-recursive-remove 'a '((a b) a (c d e a) (a a) b)) )
(print (my-recursive-remove 'a '((((a))))) )