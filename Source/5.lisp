(setq *new-list* (list))
(defun flatten(a-list)
	(loop for an-item in a-list
		do(
			if (typep an-item 'list)
				(flatten an-item)
				(push an-item *new-list*)
		)
	)
	
)

(flatten '(a b c d)) 
(print *new-list*)
(setq *new-list* (list))
(flatten '(a () b (c d))) 
(print *new-list*)
(setq *new-list* (list))
(flatten '(a (()))) 
(print *new-list*)
(setq *new-list* (list))
(flatten '(a (b c) (d e (f) ((((((g)))) h)) i) j (k) l))
(print *new-list*)
(setq *new-list* (list))